# Configure Kali Linux, My Way! (CKLMW)

###### Note: This is for my own personal use.  I take no responsibility for the applications and/or tools found within this script.

This script will give you options to install extra apps & tools on your Kali Linux.

Based off of FreelancePentester's [ddos-script](https://github.com/FreelancePentester/ddos-script "ddos-script")
